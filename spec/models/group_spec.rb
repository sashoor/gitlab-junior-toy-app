require 'rails_helper'

RSpec.describe Group, type: :model do
  let(:user) { create(:user) }
  let(:group) { create(:group) }

  let!(:group_at_level_1) { create(:group, owner: user, name: 'top_level') }
  let!(:group_at_level_2) { create(:group, owner: user, name: 'sub_level', parent_group: group_at_level_1) }
  let!(:group_at_level_3) { create(:group, owner: user, name: 'sub_sub_level', parent_group: group_at_level_2) }

  it 'is valid' do
    expect(group).to be_valid
  end

  it 'it needs a name' do
    group.name = nil
    expect(group).not_to be_valid
  end

  it 'it needs a description' do
    group.description = nil
    expect(group).not_to be_valid
  end

  it 'it needs a user' do
    group.owner_id = nil
    expect(group).not_to be_valid
  end

  it 'can have a parent' do
    expect(group_at_level_2).to be_valid
  end

  it 'can have sub groups' do
    expect(group_at_level_1.sub_groups).to include(group_at_level_2)
  end

  describe '#ancestors' do
    it 'returns all ancestors' do
      expect(group_at_level_3.ancestors).to include(group_at_level_2)
      expect(group_at_level_3.ancestors).to include(group_at_level_1)
      expect(group_at_level_2.ancestors).to include(group_at_level_1)
      expect(group_at_level_1.ancestors).to be_empty
    end
  end

  describe '#full_path' do
    it 'returns full path including parent' do
      expect(group_at_level_3.full_path).to eq('top_level / sub_level / <strong>sub_sub_level</strong>')
      expect(group_at_level_2.full_path).to include('top_level / <strong>sub_level</strong>')
      expect(group_at_level_1.full_path).to eq('<strong>top_level</strong>')
    end
  end
end
