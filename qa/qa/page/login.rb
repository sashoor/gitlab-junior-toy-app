module QA
  module Page
    class Login < Base
      def fill_email_field(email)
        fill_element(:email_field, email)
      end

      def fill_password_field(password)
        fill_element(:password_field, password)
      end

      def click_login_button
        click_element(:log_in_button)
      end
    end
  end
end
