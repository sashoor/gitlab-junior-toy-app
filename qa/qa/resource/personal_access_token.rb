module QA
  module Resource
    class PersonalAccessToken < Base
      attr_accessor :name
      attr_reader :token
      attr_writer :user

      def initialize
        @fabrication_success_text = 'New Personal Access Token created'
      end

      def user
        @user || Runtime::User.default
      end

      def fabricate_via_browser_ui!
        Flow::Login.sign_in_unless_signed_in(user: user)

        Page::Home.perform do |home|
          home.click_account_dropdown_link
          home.click_tokens_link
        end

        Page::PersonalAccessToken::Show.perform do |show|
          show.fill_name_field("personal_access-Token-#{SecureRandom.hex(4)}")
          show.click_generate_button
          @token = show.latest_token
        end
      end
    end
  end
end
