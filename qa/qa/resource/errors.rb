module QA
  module Resource
    module Errors
      ResourceFabricationFailedError = Class.new(RuntimeError)
      InconsistentStateError = Class.new(RuntimeError)
      ResourceNotDeletedError = Class.new(RuntimeError)
      ResourceNotFoundError = Class.new(RuntimeError)
      ResourceURLMissingError = Class.new(RuntimeError)
      InternalServerError = Class.new(RuntimeError)
    end
  end
end
