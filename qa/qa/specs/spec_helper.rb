require_relative '../../qa'
require 'rspec/core'
require 'rspec/expectations'
require 'capybara/rspec'
require 'webdrivers/chromedriver'

Capybara.register_driver :chrome do |app|
  options = Selenium::WebDriver::Chrome::Options.new

  selenium_options = {
    browser: :chrome,
    clear_local_storage: true,
    options: options
  }

  Capybara::Selenium::Driver.new(
    app,
    **selenium_options
  )
end

Capybara.configure do |config|
  config.default_driver = :chrome
  config.javascript_driver = :chrome
  config.app_host = QA::Runtime::Env.app_url
end

RSpec.configure do |config|
  config.define_derived_metadata(file_path: %r{/specs/features/}) do |metadata|
    metadata[:type] = :feature
  end

  config.include Capybara::DSL

  config.prepend_before do |example|

    visit('/')
  end
end
